description: Learn all about how the benefits of Multcloud deployment can reduce
  costs, maximize workflow portability & increase product delivery for your
  organization
canonical_path: /topics/multicloud
file_name: /multicloud
twitter_image: /images/opengraph/auto-devops.svg
title: Multicloud
header_body: |+
  Harnesses the power of cloud-agnostic DevOps and workflow portability.

  [Download the ebook →](https://about.gitlab.com/resources/guide-to-the-cloud/)

related_content:
  - url: /solutions/cloud-native/
    title: Learn how GitLab enables cloud native applications
  - url: /solutions/google-cloud-platform/
    title: Learn how GitLab and GCP work together
  - url: /solutions/aws/
    title: Learn how GitLab and AWS work together
cover_image: /images/topics/auto-devops.svg
body: >
  ## The need for multicloud


  Building better products with speed and consistency requires using reliable cloud solutions to rapidly scale to meet sudden demand. When businesses rely on a single vendor for cloud computing, they risk experiencing downtime and data loss if the cloud services provider is unable to meet a sudden uptick in demand. Customers expect dependable applications and few outages, making the reliance on a single cloud a risky decision for organizations looking to meet both business and market demand.


  Outages can happen, but organizations must find ways to minimize the occurrence or they risk losing customers. A multicloud approach decreases the risk of data loss and downtime by spreading computing across multiple cloud solutions, such as Google Cloud Platform, Microsoft Azure, and Amazon Web Services.


  [Download the Guide to the cloud: How to navigate the multi-cloud eBook →](/resources/guide-to-the-cloud/)


  ## What is a multicloud strategy?


  In cloud computing, a multicloud strategy is the use of at least two cloud computing services from different cloud vendors in a single network architecture. A multicloud deployment enables teams to select the best providers for every technical and business need. A multicloud environment increases the available storage, computing power, and cost savings. Organizations can choose between multiple deployments of the same type of cloud (public or private) to leverage the best cloud solutions.


  Private clouds are dedicated to one organization, so specific provisions can be made to ensure security and compliance. Private clouds can either be sold as a Platform-as-a-Service (PaaS) or offered as Infrastructure-as-a-Service (IaaS). A public cloud offers cloud solutions to multiple customers who share the cloud environment. Because they are automatically provisioned, they are considered less secure and not an option to store sensitive and confidential data.


  [Learn how CI/CD is important in a multicloud strategy →](/blog/2020/01/17/ci-cd-the-ticket-to-multicloud/)


  ## Multicloud maturity


  Approximately [85% of organizations →](https://www.ibm.com/blogs/cloud-computing/2018/10/19/survey-multicloud-management-tools/) use multicloud environments, but not every organization is at the same level of maturity. As teams work through the multicloud maturity model, they increase portability by insulating cloud services from underlying infrastructure, such as processors, operating systems, and virtualization software, through layers of abstraction.


  ### Mono-cloud


  All applications are in one cloud. With this strategy, a company goes “all-in” with one cloud provider for the ease of use, or because the services offered meet current business needs. The organization is locked in with a single vendor.


  ### No portability


  There may be separate teams within the same organization, and each is working out of different cloud providers, but each team is working in its own mono-cloud environment. This structure uses multiple clouds but is not technically multicloud.


  ### Workflow portability


  Workflow portability is what makes deploying anywhere possible. Instead of having to tailor certain workflows to certain clouds, developers can have one workflow with cloud-independent DevOps processes and frameworks for making deployment decisions.


  ### Application portability


  In this scenario, applications can run on any cloud, and cloud-specific services are abstracted. Application portability is hard to attain, because it requires engineering interfaces as abstractions. It also leaves organizations using only the features that are common to all clouds, so they miss out on any specialty capabilities that could improve their processes.


  ### Disaster Recovery Portability


  In this scenario, applications can fail over to another cloud with limited downtime. If a cloud provider’s data center should go down, organizations have the ability to switch to another provider.


  ### Workload portability


  The goal of workload portability is for organizations to shift application workloads between multiple clouds dynamically (e.g. autoscaling servers for background jobs). Workload portability makes it possible to migrate elements of a business service to the appropriate infrastructure so that it can service the needs of the user.


  ### Data portability


  Data portability is a feature that lets users take their data from a service and transfer or “port” it elsewhere, typically through an API.


  [Discover the challenges of shifting from on-prem to cloud →](/blog/2020/01/09/shifting-from-on-prem-to-cloud/)
benefits_title: Benefits of using multiple clouds
benefits:
  - title: Greater flexibility
    description: Each cloud vendor shines in some areas and is weak in others. The
      ability to work with multiple vendors lets organizations use the right
      tool for the job.
    image: /images/icons/first-look-influence.svg
  - title: Workflow portability
    description: Have a consistent workflow, regardless of where projects are deployed.
    image: /images/icons/1998-open-source-2.svg
  - title: Increased resilience
    description: Architecting failover between multiple cloud providers lets you
      stay up even if one of your vendors is down.
    image: /images/icons/concurrent-devops-dark-dark.svg
  - title: Improved cloud negotiations
    description: If another cloud vendor offers better terms or significant credits,
      businesses have better leverage because their DevOps processes are not
      tied to vendor-specific services.
    image: /images/icons/get-in-touch-icon.svg
resources_title: Next steps
resources_intro: Ready to learn more about multicloud? Here are a few resources
  to help you get started on your journey.
resources:
  - url: /webcast/cloud-native-transformation/
    title: Watch how Ask Media Group modernized their architecture and development
      with microservices, containers, and Kubernetes
    type: Webcast
  - url: https://www.youtube.com/watch?v=zV_hFcxoN8I&list=PLFGfElNsQthaaqEAb6ceZvYnZgzSM50Kg&index=3&t=0s
    title: Hear how Delta Became Truly Cloud Native - Avoiding the Vendor-Lock
    type: Webcast
