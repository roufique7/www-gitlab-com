---
layout: markdown_page
title: "Product Direction - Monitor"
description: "The Monitor stage comes after you've configured your production infrastructure and deployed your application to it. Learn more here!"
canonical_path: "/direction/monitor/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for Monitor. If you'd like to discuss this direction
directly with the product managers for Monitor, feel free to reach out to Sarah Waldner (PM of the [Health Group](/handbook/engineering/development/ops/monitor/health/)) ([GitLab](https://gitlab.com/sarahwaldner), [Email](mailto:swaldner@gitlab.com) [Zoom call](https://calendly.com/swaldner-gitlab/30min)) or Kevin Chu (Group PM of Monitor) ([GitLab](https://gitlab.com/kbychu), [Email](mailto:kchu@gitlab.com) [Zoom call](https://calendly.com/kchu-gitlab/30min)).

## Overview

The Monitor stage comes after you've configured your production infrastructure and
deployed your application to it. 

1. The Monitor stage is part of the verification and release process -
immediate performance validation helps to ensure your service(s) maintain
the expected service-level objectives ([SLO](https://en.wikipedia.org/wiki/Service-level_objective)s)
for your users.
1. The Monitor stage is an observability platform. [Observability](https://en.wikipedia.org/wiki/Observability) is the ability to infer internal states of a system based on the system’s external outputs.
Whether there are known ways to understand the total health of your systems, or your complex microservices system is full of unknowns, we want you to be able to export your system's product analytics to GitLab and use it to debug and diagnose any potential problem.
1. The Monitor stage helps you respond when things go wrong. It enables the aggregation of errors and alerts to identify problems and to find improvements.
The Monitor stage also enables responders to streamline incident response, so production issues are less frequent and severe.
1. The Monitor stage also provides is user feedback. Understanding how users experience your product and understanding how users actually use your product are critical to making the right improvements.

## Mission
The mission of the GitLab Monitor stage is to provide feedback that decreases the frequency and severity of incidents and improves operational and product performance.

The categories within the Monitor stage fits together to support the mission in the following way:

``` mermaid
stateDiagram
	Development --> Monitor: Code Deploy
  state Monitor
  {
	  s1 --> s2: Daily Operations
    s2 --> s3: Incident
    s3 --> s4: Resolution
    s2 --> s4

    s1: Verification
    s1: Metrics
    s1: DEM (Synthetics)
    s1: DEM (Web Performance Monitoring)

    s2: Observability 
    s2: Metrics
    s2: Traces
    s2: Logs
    s2: Errors

    s3: Response
    s3: Incident Management
    s3: Observability

    s4: Feedback
    s4: DEM (Real User Monitoring)
    s4: Product Analytics
  }
  Monitor --> Development: Continuous Improvement
```

## Landscape
The Monitor stage directly competes in several markets, including Application Performance Monitoring (APM), Log Management, Infrastructure Monitoring, IT Service Management (ITSM), Digital Experience Management (DEM) and Product Analytics. 
The [total addressable market for the Monitor stage was already more than $1.5 billion in 2018](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=2018046611) 
and is expected to grow as businesses continues to shift to digital.

All of these markets are well-established and crowded. 
However, they are also being disrupted by the underlying technologies used. The shift to cloud, containers, and microservices architectures changed users' expectation, and many existing vendors have struggled to keep pace. 
Successful vendors, such as market leader [Datadog](https://www.datadoghq.com/) have leveraged a platform strategy to expand their markets (such as the [acquisition of Undefined Labs to expand beyond production applications to provide code insights during development](https://drive.google.com/file/d/1hq74bZvBv5nD6Krmi-iaSsdqA9ChOf1r/view), or their expansion to [incident management in 2020](https://www.datadoghq.com/blog/dash-2020-new-feature-roundup/)), and even stages within DevOps. Competition among market leaders today is also geared toward making the whole stack observable. [New Relic's updated business model](https://blog.newrelic.com/product-news/new-relic-one-observability-made-simple/) reflects the need for vendors to capture increasing footprint (and spend) of enterprises while enabling future growth by making a significant part of their business free.

There is also an emerging recognition for the need for an integrated platform when it comes to your Monitoring capabilities. Existing players and new entrants like [Fyipe](https://gitlab.com/gitlab-com/Product/-/issues/1672) are pursuing this integrated platform approach.

The changes in the market have also revealed opportunities that new entrants into this stage, like GitLab, can take advantage of.
Specfically, the [Ops section opportunities](https://about.gitlab.com/direction/ops/#opportunities) worth re-emphasizing are:
* *Clear winner in Kubernetes:* 
Driven by software-defined infrastructure, cost management, and resiliency, organizations are [flocking to cloud-native application architectures](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view).
Kubernetes is the clear winner in container orchestration platforms.
* *Continuing commoditization of tooling:* Well funded technology companies (Google, Netflix, Lyft, Spotify etc.), meeting scaling challenges, have a history of releasing open source software that leap-frog vendor software. 
Examples include Kubernetes, Prometheus, and many of the foundational projects in CNCF. DevOps vendors have to continue to move up the value chain as previously specialized software gets commoditized. 
We believe this will continue in the Monitor stage categories. A pertinent example is that vendor instrumentation is now considered a liability with OpenTelemetry(https://opentelemetry.io) (which allows organizations to become vendor agnostics) having received buy-in from most major APM vendors.

## Principles
The Monitor stage follows GitLab's [product principles](/handbook/product/product-principles/) and the [Ops section themes](/direction/ops/#themes), and also has a few others to ensure we focus on the right experiences for our users.
- **Time is money** - When responding to incidents, every second of downtime costs our users and their businesses money. It's important to make interactions in Gitlab crisp and focused on resolution.
- **Make setup easy** - Monitor tools will never be used if they are difficult to setup. Whether that is instrumentation of agents, or configuration of alerts. Having a strong bias for convention over configuration to make this process quick is critical. 

## Vision
The vision of the Monitor stage is to enable DevOps team to operate their application by enabling verification, observability, incident response, and feedback all within GitLab. This vision is part of the overall [GitLab vision](https://about.gitlab.com/direction/#vision) and enables teams to complete the DevOps loop.

GitLab is uniquely qualified to deliver on this bold and [ambitious](/handbook/product/product-principles/#how-this-impacts-planning) vision because:

1. GitLab is a complete devops tool that is connected across the devops stages. Being one tool makes the circular devops workflow, and feedback, seamless and achievable.
2. The Monitor stage is pursuing a differentiated strategy from other observability vendors by not pursuing a usage based model business model by charging for processing and storage of observability.
Instead, we lean on powerful open source software, such as Prometheus and OpenTelemetry, along with commodity cloud services to enable customers to setup and operate Monitor stage observability solutions effectively. 
We will be successful because we are well-practiced in integrating different parts of the tool chain together.
3. Going cloud-native is a disruption to operations as usual. Cloud-native systems are constantly changing, are ephemeral, and are complex. 
As more and more companies adopt cloud-native, GitLab can create a well-integrated central control-pane that enables broad adoption by building on top of the tools that cloud-native teams are already familiar with and are using.

A trade-off in our approach is that we are explicitly not striving to be a fully turn-key experience that can be used to monitor all applications, particularly legacy applications.
Wholesale removing an existing monitoring solution is painful and a land and expand strategy is prudent here. As a customer recently explained, "Every greenfield application that we can deploy with your monitoring tools saves us money on New Relic licenses."
 
As this stage matures, we will begin to shift our attention and compete more directly with incumbent players as a holistic Monitoring solution for modern applications.

### Strategy
To achieve our vision, our strategy is to:

* Focus first on user adoption and dogfooding of Incident Management
* Partner on observability. Make integration with observability vendors easy for GitLab users
* Identify open source tools that can complement/augment out-of-the-box GitLab Monitor capabilities and rely on community contribution

### Pricing
<%= partial("direction/ops/tiering/monitor") %>

## What's next

The Monitor surface area is large. Rather than continue to pursue bringing multiple products within the monitor purview to market concurrently, GitLab has consolidated the majority of its focus to Incident Management. This allows us to complete the [smart feedback loop](/direction/ops/#smart-feedback-loop) within a single DevOps platform as a first priority. With GitLab Incident Management's development timeline, our users will benefit from the advantage of enabling collaboration for incident response within the same tool as their source code management, CI/CD, plan, and release workflows - all within the same tool. This most effectively positions GitLab to gain market traction and user adoption. 

We still intend to invest in the Observability categories which include Metrics, Logging, and Tracing and this is top of mind. We need to be strategic as the Monitor team has limited bandwidth. In FY21Q1, we are working on a strategy that will allow us to start investing in the Observabitlity categories without slowing down on Incident Management. Check out more details on this [epic](https://gitlab.com/groups/gitlab-org/-/epics/5132).

The Monitor stage's goals from 2021-01 through 2021-04 are the following:

1. Mature the Incident Management category so that the GitLab SRE team can dogfood it
 * Key Result 1: [plan, execute, and collect feedback on GitLab's third game-day](https://gitlab.com/gitlab-org/gitlab/-/issues/285284)
 * Key Result 2: SRE team is using Incidents in place of Issues
 * Key Result 3: Release minimal version of [on-call schedule management](https://gitlab.com/groups/gitlab-org/-/epics/3960)

2. Grow estimated SMAU for Monitor to 15,000 users (target for FY20Q4 was 12,000 users and was achieved)


You can see our entire public backlog for Monitor at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Monitoring);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

## Performance Indicators (PIs)

Our [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) for the Monitor stage is the [Monitor
SMAU](https://app.periscopedata.com/app/gitlab/758607/Centralized-SMAU-GMAU-Dashboard?widget=10039330&udv=0) ([stage monthly active users](https://about.gitlab.com/handbook/product/metrics/#monthly-active-users-mau)).

As of December 2020, the Monitor SMAU is based on the [Incident Management category](https://about.gitlab.com/direction/monitor/debugging_and_health/incident_management/). It is the count of unique users that interact with alerts and incidents. This PI will inform us if we are on the right path to provide meaningful incident response tools.

<embed width="100%" height="100%" style="min-height:300px;" src= "<%= signed_periscope_url(chart: 10132068, dashboard: 769975, embed: 'v2') %>">

<%= partial("direction/workflows", :locals => { :stageKey => "monitor" }) %>

<%= partial("direction/categories", :locals => { :stageKey => "monitor" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "monitor" }) %>
