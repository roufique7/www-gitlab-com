---
layout: job_family_page
title: "Tax Manager"
---

## International Tax Manager 

The International Tax Manager adds value to GitLab’s overall tax strategy including all components of tax: meaning tax planning, tax compliance, tax assurance and accounting for income taxes. They are a seasoned all-rounder. International Tax Manager reports to the Director of Tax and is part of an awesome Finance Team in an all-remote, demanding high-growth environment.

### Job Grade 

The International Tax Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Play a significant role in establishing GitLab’s global tax strategy
* Tax Planning
  * Support the implementation of non-structural tax planning strategies
  * Participate in proposed Transactions to advise the Finance Team on Tax Consequences
  * Support USA and foreign tax integration of acquisitions, dispositions and restructurings into the corporate structure
* Transfer Pricing
  * Ensure adherence to the Transfer Pricing Concept
  * Maintain Transfer Pricing Documentation
* Tax Compliance
  * Ensure Global Corporate / State Income Tax Compliance
  * Closely work together with finance team members on VAT, Sales, Use Tax compliance
  * Ensure Global VAT compliance to electronic services
  * Take ownership of the control cycle to mitigate risks of Permanent Establishments
  * Take ownership of the cycle to mitigate employment tax risks
* Tax Audits
  * Evaluate requests and participate in developing strategies
  * Support the coordination and implementation of tax audit strategies
* Financial Reporting
  * Support preparation current / deferred income tax expense for financial statement purposes
  * Evaluate uncertain tax reporting positions and business combinations
  * Analyze the tax aspects of financial projections and forecasts
  * Lead technology and process improvements to ensure accurate, timely information is provided to the Finance team
  * Implementation and assurance of control cycle compliant with Sarbanes-Oxley
* Departmental liaison with IT staff on technical matters relating to tax applications

### Requirements 

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* 5-8 years experience in a Big 4 environment and/or Business environment
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* Working knowledge of multiple International tax areas
* Ability to managing multiple projects at the same time
* Ability to use GitLab

## Senior International Tax Manager

### Job Grade 

The Senior International Tax Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements 

* Degree in Business Taxation (Master's / JD / CPA)
* Proven track record of personal growth in your career path
* 8-12 years experience in a Big 4 environment and Business environment
* Affinity with Software and/or SaaS in a high growth environment
* Demonstrate a hands-on approach and success in working in a team environment
* Successfully leading large projects and/or change management projects
* Strong influencing skills, high adaptability and analytical thinking
* Expert in multiple International tax areas
* Leading multiple projects at the same time
* Ability to use GitLab

### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 45 minute interview with the Hiring Manager
* Candidates will then be invited to schedule a 30 minute interview with our Principal Accounting Officer 
* Next, candidates will be invited to schedule a 30 minute interview with a Senior Technical Accounting Manager and our Payroll Lead 
* Finally, candidates may have a 45 minute call with the CFO

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## US Tax Manager

The US Tax Manager adds value to GitLab’s overall tax strategy including all components of tax: meaning tax planning, tax compliance, tax assurance and accounting for income taxes. They are a seasoned all-rounder with a US tax background. The US Tax Manager reports to the Director of Tax and is part of an awesome Tax and Finance Team in an all-remote, demanding high-growth environment.

### Job Grade
The US Tax Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities
- Contribute to GitLab’s Global Tax Strategy
- Contribute to Global Tax Planning Strategies
- Contribute to USA Tax integration of acquisitions, dispositions and restructurings
- Contribute to the adherence to GitLab’s Transfer Pricing Model
- Ownership of US Federal, State and Local Tax Compliance
- Collaborate with our Tax Accountant on US GST Compliance
- Contribute to the mitigation of Permanent Establishment exposure
- Contribute to the mitigation of Employment Tax exposure
- Contribute to US Tax Audits and participate in developing strategies
- Contribute to the US and Foreign Tax Provisioning
- Contribute to the maintenance of GitLab’s Global Corporate Structure
- Evaluate and perform calculations for uncertain tax position accruals (e.g. FIN48, FAS5)
- Analyze the tax aspects of GitLab’s Financial Model (e.g. BEAT, GILTI, FDII)
- Collaborate with Global Sales Team to Withholding Tax Related Questions
- Collaborate with Payroll & Stock Administrator on US tax implications of stock options 
- Contribute to technology improvements to ensure fast and accurate data processing
- Implementation and assurance of control cycle compliant with SOX
- Collaborate with our IT department for tax settings of Finance IT Systems (e.g. ERP)
- Increase efficiency for existing processes and design new processes (e.g. use of AI)
- Contribute to GitLab’s international expansion activities (e.g. entity setup)
- Collaborate with GitLab Team Members on day-to-day activities, requests and projects
- Contribute to the overall finance & tax OKR’s
- This position reports to the Director of Tax

### Requirements
- Degree in Business Taxation (Master's / JD / CPA)
- Proven track record of personal growth in your career path
- 5-8 years experience in a Big 4 environment and/or Business environment
- Affinity with Software and/or SaaS in a high growth environment
- Demonstrate a hands-on approach and success in working in a team environment
- Working knowledge of US Tax in an International Tax environment
- Ability to managing multiple projects at the same time
- Ability to use GitLab

## Senior US Tax Manager

### Job Grade
The Senior US Tax Manager is a  [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Requirements
- Degree in Business Taxation (Master's / JD / CPA)
- Proven track record of personal growth in your career path
- 8-12 years experience in a Big 4 environment and Business environment
- Affinity with Software and/or SaaS in a high growth environment
- Demonstrate a hands-on approach and success in working in a team environment
- Successfully leading large projects and/or change management projects
- Strong influencing skills, high adaptability and analytical thinking
- Expert in US Tax and International tax 
- Leading multiple projects at the same time
- Ability to use GitLab

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process.
* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- 45 minute interview with the Director of Tax
- 30 minute interview with our International Tax Manager
- 30 minute interview with our Tax Accountant
- 30 minute interview with our Sr. Director, Controller

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Performance Indicators

* [Effective Tax Rate](https://about.gitlab.com/handbook/tax/performance-indicators/#effective-tax-rate-etr)
* [Budget vs. Actual](https://about.gitlab.com/handbook/tax/performance-indicators/#budget-vs-actual)
* [Audit Adjustments](https://about.gitlab.com/handbook/tax/performance-indicators/#audit-adjustments)
* [International Expansion](https://about.gitlab.com/handbook/tax/performance-indicators/#international-expansion)

## Career Ladder
The next step in the Manager, Tax job family is to move to the [Director, Tax](/job-families/finance/director-of-tax/) job family.
