---
layout: handbook-page-toc
title: Mental Health Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Overview

The Learning and Development team at GitLab is working to educate the team on how they can [manage burnout](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/24). This quarterly newsletter is part of a larger effort to increase conversation about taking care of our mental health, increasing awareness of existing resources available to the GitLab team, and enable managers to support their team.

### Long term goals

1. Normalize the discussion about mental health across the GitLab team.
1. Increase access to and awareness of existing mental health resources for the GitLab team.
1. Understand how our asynchronous culture can contribute to improved mental health for team members, specifically by decreasing the total number of synchronous meetings for team members.
1. Increase documentation about mental health management resources in our handbook by collaborating and sharing strategies and tools that work

### Ideas for future newsletters

Below is a running list of ideas and topics that can be incorporated into this quarterly newsletter. If you have ideas of what you'd like to see, please open a merge request and add to this page!

1. Leadership discussions about mental wellness and what mental wellness means to them, how it has impacted them in the past and what they have learnt.
1. Interview with other team members to dicuss strategies and experiences
1. Meditation resources
1. Journal prompts and tools
1. Book clubs or suggested outside readings or podcasts

## Newsletter process

This newsletter is shared on the third to last Thursday of each quarter. This cadence comes 3 weeks before the [L&D newsletter](https://about.gitlab.com/handbook/people-group/learning-and-development/newsletter/) is shared, to avoid confusion or an overload of information.

The newsletter is planned using both issues in the [Mental Health project](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health) and merge requests to the `www-gitlab-com` project. The planning process includes the following steps:

1. Open a WIP merge request to begin a new newsletter page 3 weeks before publish date
1. Open an issue to hold discussion and feedback related to the newsletter
1. Relevant stakeholders/contributors are tagged in the merge request each month to provide content or review suggested content in the outline
1. Reviewers provide feedback no later than 3 pm CT two business days before the planned go-live date to allow time for revisions
1. L&D Team announces the newsletter via slack on go-live date.
1. Once the newsletter goes live, the L&D Team sends a slack notification in the #whats-happening-at-gitlab, #learninganddevelopment, and #managers Slack channels

## Past newsletters

Past mental health newsletters will be linked here. Our first newsletter is planned for FY21-Q4, with a target publish date of 2020-01-14.

## Merge request template

```
---
layout: handbook-page-toc
title: {add date - ex. FY21-Q3} L&D Mental Health Newsletter
---

{brief intro}


## GitLab Resource Feature

{Highlight a health benefit from GitLab related to mental health, with a focus on Modern Health and our PTO policy}


## Taking time for mental health

{Highlight additional external or internal resources team members can use to manage mental health}


## Leadership feature 

{Emebd short recorded discussion with leadership or another GitLab team member about burnout management}

## Manager resources

{Highlight resources managers can use to enable their team in supporting their mental health}

## Team member resources

## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](). 

```
