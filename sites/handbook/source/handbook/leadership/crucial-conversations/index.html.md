---
layout: handbook-page-toc
title: Crucial Conversations
description: “GitLab’s strategies for being an effective leader during crucial conversations with team members”
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# Crucial Conversations at GitLab

Any synchronous or asynchronous engagement with team members may turn into a crucial conversation. At GitLab, we can develop the skills of sensing the tone of an async or sync conversation to uncover potential pain-points, risks, blockers, etc for team members. We need to find a way to create [psychological safety](/handbook/leadership/emotional-intelligence/psychological-safety/#introduction) for our people. Using a 1-1 can be a great way to gain context on a situation a team member is facing and hold a crucial conversation. 

What is a Crucial Conversation: According to the [book](https://www.amazon.com/Crucial-Conversations-Talking-Stakes-Second/dp/0071771328/ref=sr_1_1?dchild=1&keywords=crucial+conversation&qid=1605712836&sr=8-1), a conversation to be crucial is that the results of it could have a huge impact on the quality of your life. A conversation involving a promotion, performance, debate between coworkers, etc. In short crucial conversations are discussions between two or more people where: 
1. Stakes are high
2. Opinion vary
3. Emotions run strong

Most of us are good at avoiding them because it's human nature to avoid pain and discomfort when addressing a crucial conversation. So what do you do when faced with a crucial conversation?
1. We can avoid them
2. We can face them and them poorly
3. We can them and handle them well

At GitLab, there can be many instances where a crucial conversation is needed. Whether it is addressing the underperformance of a team member, discussing the results of promotion, interviewing, being notified that a high performer is leaving the team, etc. The Crucial Conversations book has strategies laid out that can help master one:
1. **Power of Dialogue:** Gaining information and context on the situation. The free flow of meaning between two or more people. 
2. **Shared Meaning:** Each of us enters conversations with our own opinions, feelings, theories, and experiences about the topic at hand. Coming to a shared opinion and entering a pool of a shared meaning helps our synergy with team members in many ways. 
3. **Start with the Heart:** If you can’t get yourself right, you’ll have a hard time getting dialogue right by looking inward first. Apply empathy in the moment by asking questions to yourself that return you to the dialogue:
   - What do I really want for myself?
   - What do I really want for others?
   - What do I really want for the relationship?
   - How would I behave if I really wanted these results?
4. **Learn to Look - How to Notice When Safety is at Risk:** It can be difficult to see what exactly is going on and why during crucial conversations. Watch for these situations to learn to spot one: 
   - The moment a conversation turn crucial
   - Signs that people feel safe
   - Your own style under stress
5. **Make It Safe:** Nothing kills dialogue than fear. When you make it safe, you can talk about almost anything and people will listen. If you spot safety risks as they happen, you can step out of the conversation, build safety, and then find a way to dialogue about almost anything.
6. **Master Your Stories:** Learn to exert influence over your feelings by slowing down the storytelling process, take a step back, and retrace your path to action one element at a time.

These are just a few strategies outlined in the Crucial Conversation book. We [highly recommend reading it](https://about.gitlab.com/handbook/leadership/#books) for leading teams at GitLab. 

## Strategies for Successful Crucial Conversations

### 9 Influencing Strategies

The ability to influence is an essential leadership skill. To influence is to have an impact on the behaviors, attitudes, opinions and choices of your team members and others across GitLab and externally. Influence should not be confused with power or control. It is also not about manipulating others to get your own way. It is about noticing what motivates team members commitment and using that knowledge to leverage performance and results.  

[Leadership](/handbook/leadership/) has sometimes been described as the ability to influence others. An effective leader does not move team members into action by coercion. An effective leaders will articulate the overall vision and goals for the organization. By doing such this can motivate and move team members to action by tapping into their desires and need for success. Positive influence that is properly channeled can also bring about transformation and change for team members, department, division and the company. A leader that exhibits and exerts positive influence in others will build trust and become a true driving force towards transparency, iteration, collaboration and results.

There are many different influencing strategies and in this section we are going to review 9 that leaders can review. Each strategy below will include a definition, example and ways for leaders to develop this strategy.
 

1.  **Empowerment**: Making others feel valued by involving them in decision making and giving them recognition. 
    * Example: Manager invites their team members to a meeting in order to take their inputs on how to improve product quality.
    * Develop: Finding win-win situations, not taking people for granted, not providing unsolicited advice to people and having confidence in the ability of others.
1.  **Interpersonal Awareness**: Being able to recognize and address the concerns of key stakeholders.
    * Example: A manager/leaders offers support to team members by identifying a list of questions that may be asked in meetings with senior leadership.
    * Develop: Awareness of verbal and non verbal cues, putting yourself in another person's shoes and testing your own understanding of the message.
1.  **Negotiating**: Gaing support from others by mutually agreeing on a common set of goals or outcomes.
    * Example: A vendor who is trying to land a big deal may offer their client incentives or a discount provided the size of the order large enough.
    * Develop: Understand the other person's requirements, identify your non-negotiable, role-play wiht a colleague, practive in a low risk real life situation.
1.  **Networking**: Establish and maintain a network of contacts who you may need to influence going forward.
    * Example: A manager spends time with their peers or cross functional partners in different activites (either work or non work related) in order to develop a better bond/relationship with their network.
    * Develop: Take interest in other peoples lives, find common ground, leverage existing relationships and take advantage of existing opportunities.
1.  **Stakeholder awareness**: Identifying the key stakeholder and the ability to gain support when needed.
    * Example: A manager identifies the key stakeholder and decision maker within a supporting organization and regularly meets with them to collaborate.
    * Develop: Keep your ear to the ground to understand what is happening around you, learn by example, identify the decision makers, appreciate other's points of view.
1.  **Shared vision**: Showing others how your ideas support the broader organizational vision.
    * Example: A manager communicates their vision to their team by helping them understand what they can become and the steps needed to achieve the goal.
    * Develop: Always keep the outcomes in sight, speak about the benefits of the approach and get others to participate.
1.  **Impact and Influence**: Choosing the most appropriate time and manner to present your point, always keeping in mind your audience.
    * Example: A manager understands the landscape or demographics of their audience and is thoughtful in their presentation approach.
    * Develop: Study prominent influencers, try different approaches each time, take formal training, model good influencers within the organization.
1.  **Analytical reasoning**: Using analytical reasoning to convince others about your point of view.
    * Example: A team member uses data and market insights to convince their manager about their ideas.
    * Develop: Have more than 1 reason for your idea, clearly lay out the pros and cons, show clarity of thought and structure, spend time on research and case studies. 
1.  **Coercion**: Using threats or pressure tactics to get other to agree to your point or to comply to rules.
    *  Example: A manager makes a decision with no or little input from team and informs team members of actions or consequences if they do not comply. 
    *  Develop: Use it sparingly and not as the first resort, leave no room for ambiguity and do not use as a way to surpress your team member or others.

### Additional resources

Below are additional resources on influencing and leadership for you to review.

* [What Great Leaders Know about Influence](https://www.forbes.com/sites/rebeccanewton/2016/07/27/six-steps-to-increase-your-influence/#5609705a1edd)
* [Influencing Skills: A Key to Leadership Success!](https://www.linkedin.com/pulse/influencing-skills-key-leadership-success-marcia-zidle-ms-bcc/)
* [Influencing Others: A Key Leadership Skill](https://www.ginaabudi.com/influencing-others-a-key-leadership-skill/)
* [The 5 Key Skills of Influential Leaders Within Every Organization](https://www.inspirationaldevelopment.com/5-key-skills-influential-leaders-every-organisation/)
* [Influence and Leadership](http://www.theelementsofpower.com/power-and-influence-blog/influence-and-leadership/)
* [5 Leadership Strategies Proven to Improve Performance on Your Team](https://crestcomleadership.com/2016/12/01/5-leadership-strategies-to-improve-performance-in-your-company/)
* [The 7 Best Books to Improve Influencing Skills](https://www.roffeypark.com/influencing-skills/the-7-best-books-to-improve-influencing-skills/)
* [7 Ways to Build Influence in the Workplace](https://www.inc.com/jayson-demers/7-ways-to-build-influence-in-the-workplace.html)

