---
layout: markdown_page
title: "Sarah Chia's README"
---

## Sarah's README

## 2 Truths and a Lie
- I have 3 kids with 3 different fathers.
- I have 3 kids all with the same dad.
- I have lived in Texas my whole life.

## Current Role & Goals
I provide paralegal support to the Legal, Privacy and Product team.

This includes a wide variety of tasks/responsibilities, such as conducting vendor privacy reviews, developing legally compliant internal processes, approving DMCA requests, and responding to law enforcement subpoena requests. It also includes conducting legal research and recommending solutions based on relevant law.

### My current goals are:
1. To create legally compliant processes that support GitLab’s efficiency value
1. To be an inclusive and reliable team member who others can confidently know will listen to their point of view and work toward solutions where everyone involved is respected and supported
1. To increase my own knowledge on the subject matter of my team’s specialty
1. To increase my understanding of Git, haml/yaml, and what’s happening under the hood of our product

## My Working Style
According to 16 Personalities, I’m an [Advocate](https://www.16personalities.com/infj-personality) [(Assertive type)](https://www.16personalities.com/articles/assertive-advocate-infj-a-vs-turbulent-advocate-infj-t), and their [workplace description](https://www.16personalities.com/infjs-at-work) is pretty spot on. 

##Strengths & Areas of Improvement

### Strengths:
1. [Growth mindset](https://about.gitlab.com/handbook/values/#growth-mindset) - Though I excelled at traditional academics, the school setting was mostly a deterrent to developing a love for learning. As an adult, however, I have come to value curiosity and understanding how the world works (or doesn’t!). Because of this, I bring a true curiosity to my job and really enjoy the opportunities I have to dig into problems and research for solutions.
1. [Small steps](https://about.gitlab.com/handbook/values/#minimal-viable-change-mvc) - Iteration is one of my favorite values at GitLab, maybe because I tend to be good at breaking down complicated issues into manageable pieces. I still have a lot to learn about finding the true MVC, but this is an exercise that I enjoy.
1. [Process improvement](https://about.gitlab.com/handbook/values/#focus-on-improvement) and [Blameless problem solving](https://about.gitlab.com/handbook/values/#focus-on-improvement) - I am able to imagine how individual pieces can fit together to make a cohesive system. I enjoy considering current processes to understand what isn’t working about them and offer proposals for solutions that are empathetic to those involved while increasing efficiency.
1. [Kindness](https://about.gitlab.com/handbook/values/#kindness) - Communicating with respect is important to me because I have a deep personal value that dignity is inherent in all humans. Because of this, I also enjoy helping others reach their goals and encouraging others through positive validation.

### Areas for Improvement:
1. [Communicating efficiently](https://about.gitlab.com/handbook/values/#short-verbal-answers) - In the interest of respecting other team members’ time, I am working on clear and concise communication in order to help move projects forward with sufficient detail that is not overly verbose. (See communication section below for one of the ways I’m working on ths.)
1. [Tenacity](https://about.gitlab.com/handbook/values/#tenacity) - One of GitLab’s sub-values for Results is Tenacity. As someone who is emotionally even-keeled, I sometimes have difficulty conjuring up feelings of motivation and passion when faced with lingering adversity. I combat this in a few different ways, which include shifting to a reliance on logical understanding rather than emotional enthusiasm and being intentional about self-care in my free time.
1. [Low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame) - The Legal field is full of technicalities, so perfect work product is often a requirement. At GitLab, our values of iteration and collaboration mean that we show others our imperfect ideas and solutions. This is hard, but ultimately a good exercise in humility and open mindedness. 
1. [Directness](https://about.gitlab.com/handbook/values/#directness) - Considering the amount of time I have already spent on trying to word this, I’m going to just say that this is hard for me. Schedule a coffee call and ask me about “Register Sarah” for a good laugh about what happens when I try to be direct. 


## How I Communicate / The Best Ways to Communicate With me
A *confidential* issue is the best place to communicate with me. As you see my work in issues, you may notice that bulleted lists are a go-to for me. I use these because they help ensure that I am making clear points and/or asking specific questions. If I ask you questions in a bulleted list, one of the nicest things you can do for me is to respond in like fashion so I know that I am reading your response accurately.
